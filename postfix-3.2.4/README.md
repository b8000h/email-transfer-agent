https://wiki.centos.org/HowTos/postfix

port 修改
在master.cf里的非#号开头的第一行应该是smtp inet n － n －－smtpd
这一行表示的是smtp的进程，和/etc/services里的
smtp 25/tcp mail
smtp 25/udp mail
相对应
再在后追加一行同样的进行，修改一下前面的名字，如
smtp2 inet n －n － － smtpd，
保存后，在/etc/services里也相应增加一个服务器进程端口号即可，如：
smtp2 26/tcp mail2
smtp2 26/udp mail2
保存后，reload不行，要restart postfix就应该能同时启动25和26两个功能完全相同的端口。
OK了


postfix的产生是为了替代传统的sendmail.
相较于sendmail,postfix在速度，性能和稳定性上都更胜一筹。现在目前非常多的主流邮件服务其实都在采用postfix. 

2. 更快：
    postfix在性能上大约比sendmail快三倍。一部运行postfix的台式PC每天可以收发上百万封邮件。
3. 兼容性好:
    postfix是sendmail兼容的，从而使sendmail用户可以很方便地迁移到postfix。Postfix支持/var[/spool]/mail、/etc/aliases、 NIS、和 ~/.forward 文件。
   4. 更健壮：
postfix被设计成在重负荷之下仍然可以正常工作。当系统运行超出了可用的内存或磁盘空间时，postfix会自动减少运行进程的数目。当处理的邮件数目增长时，postfix运行的进程不会跟着增加。
5. 更灵活：
postfix是由超过一打的小程序组成的，每个程序完成特定的功能。你可以
通过配置文件设置每个程序的运行参数。
   6. 安全性
postfix具有多层防御结构，可以有效地抵御恶意入侵者。如大多数的postfix程序可以运行在较低的权限之下，不可以通过网络访问安全性相关的本地投递程序等等。
下面来介绍Linux上如何搭建和配置postfix服务：
1.关掉sendmail相关的所有服务，最好是直接卸载sendmail.
# service sendmail stop
# chkconfig sendmail off
#rpm -qa | grep sendmail | xargs rpm -e
2.安装postfix.
redhat6.0以上版本应该是默认集成了postfix服务的，假如没有安装的话，可以手动安装。
rpm -qa | grep postifx (查看是否安装)
yum install postfix
3.安装完成后，修改配置文件：/etc/postfix/main.cfg
vi /etc/postfix/main.cf
myhostname = sample.test.com　 ← 设置系统的主机名
mydomain = test.com　 ← 设置域名（我们将让此处设置将成为E-mail地址“@”后面的部分）
myorigin = $mydomain　 ← 将发信地址“@”后面的部分设置为域名（非系统主机名）
inet_interfaces = all　 ← 接受来自所有网络的请求
mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain　 ← 指定发给本地邮件的域名
home_mailbox = Maildir/　 ← 指定用户邮箱目录
<保存退出！>
4.为本机添加DNS server.
为什DNS Server？因为在邮件发送过程中，需要把邮件地址的domain地址转化成IP地址，再去发送给对应的收件人，这里涉及到DNS中的A记录和MX记录相关的知识，不熟悉的同学可以google或者百度 脑补一下 :-)
如何添加DNS server呢，DNS server去哪里寻找？
vim /etc/resolv.conf 
添加如下行:
nameserver 8.8.8.8
nameserver 8.8.4.4
上面用的8.8.8.8/8.8.4.4是Google Free DNS server,当然还有很多免费的DNS server供大家使用，可以google一下:-)
5.测试一下邮件是否能够发送成功：
命令行输入$: > echo "Mail Content" | mail -s "Mail Subject" xxxx@xxx.com
Note:if you see below warings after you run above command.
send-mail: warning: inet_protocols: IPv6 support is disabled: Address family not supported by protocol
send-mail: warning: inet_protocols: configuring for IPv4 support only
postdrop: warning: inet_protocols: IPv6 support is disabled: Address family not supported by protocol
postdrop: warning: inet_protocols: configuring for IPv4 support only
that means you don't have IPv6 configured in your OS's network stack, but your mailer (presumably postfix) is configured to use IPv6. Since there is no IPv6 for your mailer to use, it's warning you that it's only going to use IPv4.

To disable the waring messsage, Go to /etc/postfix/main.cf and change from:
inet_protocols = all
to:
inet_protocols = ipv4
This will only use ipv4 and the warning message will go away.
You will have to issue a stop and start for postfix to register the change. 
service postfix restart
6.查看log，确认邮件发送状态：
Postfix邮件的log位置是：/var/log/maillog
发送成功的话，会返回250和OK，也可以去自己的邮件客户端查收。
一切OK的话，那Postfix mail service应该就搭建成功了。
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
另外一些有用的postfix维护命令，一遍日常的检测和维护：
mailq :会列出当前在postfix发送队列中的所有邮件
postsuper -d ALL:删除当前等待发送队列的所有邮件，包括发送失败的退信
当然还有很多，就不一一列举了，大家可以网上搜索扩展，Good Luck!
7.Update Mail From sender
echo "do-not-reply@example.com root@example.com" >> /etc/postfix/generic 
echo "smtp_generic_maps = hash:/etc/postfix/generic" >>/etc/postfix/main.cf 
postmap /etc/postfix/generic
service postfix restart



postfix 的 maillog
注意看 port 25
```
[root@iZrj9f7oun82tfzuuu7t6rZ ~]# tail -20 /var/log/maillog
May 26 10:10:01 debug010000002015 postfix/pickup[16935]: BEBF3180AED: uid=500 from=<root>
May 26 10:10:01 debug010000002015 postfix/cleanup[16869]: BEBF3180AED: message-id=<20170526021001.BEBF3180AED@iZrj9f7oun82tfzuuu7t6rZ.localdomain>
May 26 10:10:01 debug010000002015 postfix/qmgr[4138]: BEBF3180AED: from=<root@iZrj9f7oun82tfzuuu7t6rZ.localdomain>, size=758, nrcpt=1 (queue active)
May 26 10:10:01 debug010000002015 postfix/local[16881]: BEBF3180AED: to=<httpd@iZrj9f7oun82tfzuuu7t6rZ.localdomain>, orig_to=<httpd>, relay=local, delay=0.04, delays=0.03/0/0/0, dsn=2.0.0, status=sent (delivered to mailbox)
May 26 10:10:01 debug010000002015 postfix/qmgr[4138]: BEBF3180AED: removed
May 26 10:11:01 debug010000002015 postfix/pickup[16935]: C87EE180AED: uid=500 from=<root>
May 26 10:11:01 debug010000002015 postfix/cleanup[16869]: C87EE180AED: message-id=<20170526021101.C87EE180AED@iZrj9f7oun82tfzuuu7t6rZ.localdomain>
May 26 10:11:01 debug010000002015 postfix/qmgr[4138]: C87EE180AED: from=<root@iZrj9f7oun82tfzuuu7t6rZ.localdomain>, size=758, nrcpt=1 (queue active)
May 26 10:11:01 debug010000002015 postfix/local[16881]: C87EE180AED: to=<httpd@iZrj9f7oun82tfzuuu7t6rZ.localdomain>, orig_to=<httpd>, relay=local, delay=0.03, delays=0.02/0/0/0, dsn=2.0.0, status=sent (delivered to mailbox)
May 26 10:11:01 debug010000002015 postfix/qmgr[4138]: C87EE180AED: removed
May 26 10:11:23 debug010000002015 postfix/qmgr[4138]: 3B220180AE4: from=<httpd@iZrj9f7oun82tfzuuu7t6rZ.localdomain>, size=10421, nrcpt=1 (queue active)
May 26 10:11:53 debug010000002015 postfix/smtp[17182]: connect to gmail-smtp-in.l.google.com[74.125.28.26]:25: Connection timed out
May 26 10:12:01 debug010000002015 postfix/pickup[16935]: CEC2F180AED: uid=500 from=<root>
May 26 10:12:01 debug010000002015 postfix/cleanup[16869]: CEC2F180AED: message-id=<20170526021201.CEC2F180AED@iZrj9f7oun82tfzuuu7t6rZ.localdomain>
May 26 10:12:01 debug010000002015 postfix/qmgr[4138]: CEC2F180AED: from=<root@iZrj9f7oun82tfzuuu7t6rZ.localdomain>, size=758, nrcpt=1 (queue active)
May 26 10:12:01 debug010000002015 postfix/local[16881]: CEC2F180AED: to=<httpd@iZrj9f7oun82tfzuuu7t6rZ.localdomain>, orig_to=<httpd>, relay=local, delay=0.02, delays=0.01/0/0/0, dsn=2.0.0, status=sent (delivered to mailbox)
May 26 10:12:01 debug010000002015 postfix/qmgr[4138]: CEC2F180AED: removed
May 26 10:12:23 debug010000002015 postfix/smtp[17182]: connect to alt1.gmail-smtp-in.l.google.com[74.125.69.26]:25: Connection timed out
```